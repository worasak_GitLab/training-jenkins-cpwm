*** Settings ***
Library           Selenium2Library
Library           Collections
Library           String
Library           BuiltIn
Library           DateTime
Library           OperatingSystem
Library           DatabaseLibrary
Library           Process
Library           XML
Library           AutoItLibrary
Library           Process

*** Variables ***
${pipeline}       DooDi    # ,AIS_Startup_Startup,AIS_Startup_Playground,AIS_Startup_SystemAdmin

*** Test Cases ***
GetLogData
    [Documentation]    research to keyword runprocess \ \ http://www.jspringbot.org/library-process.html
    @{jenkins_pipeline}    String.Split String    ${pipeline}    ,
    Log List    ${jenkins_pipeline}
    @{list_xmlpathlog}    Get Path RobotFramework Log
    ${loop_index}    Set Variable    0
    ${JSONdic}    BuiltIn.Create Dictionary
    ${DicReturn}    BuiltIn.Create Dictionary
    ${listTestsuite}    BuiltIn.Create List
    ${TestSuiteData}    BuiltIn.Create List
    : FOR    ${folder}    IN    @{list_xmlpathlog}
    \    ${index}    Evaluate    ${loop_index}+1
    # check error
    \    ${xml_file_status}    BuiltIn.Run Keyword And Return Status    OperatingSystem.File Should Exist    ${folder}
    \    ${folder}    OperatingSystem.Normalize Path    ${folder}
    \    ${XML}    BuiltIn.Run Keyword If    ${True} == ${xml_file_status}    XML.Parse Xml    ${folder}
    \    ...    ELSE    BuiltIn.Continue For Loop
    \    Comment    ${XML}    XML.Parse Xml    ${folder}
    \    ${testsuite_name}    ${testsuite_pass}    ${testsuite_fail}    ${testsuite_total}    ${testsuite_executedate}    Get Testsuite Status
    \    ...    ${XML}
    \    ${testcase_detail}    Get Testcase Name    ${XML}    ${testsuite_name}
    \    ${index}    BuiltIn.Create Dictionary
    \    Collections.Set To Dictionary    ${index}    TestsuiteName    ${testsuite_name}
    \    Collections.Set To Dictionary    ${index}    TotalTestcase    ${testsuite_total}
    \    Collections.Set To Dictionary    ${index}    AllPass    ${testsuite_pass}
    \    Collections.Set To Dictionary    ${index}    AllFail    ${testsuite_fail}
    \    Collections.Set To Dictionary    ${index}    ExecuteDate    ${testsuite_executedate}
    \    Append To List    ${listTestsuite}    ${index}
    \    ${TestSuiteData}    Combine Lists    ${TestSuiteData}    ${testcase_detail}
    \    log list    ${TestSuiteData}
    ${summary_testsuite}    ${summary_testcase}    ${summary_testcasepass}    ${summary_testcasefail}    Get Summary Testsuite    @{listTestsuite}
    log list    ${listTestsuite}
    log list    ${TestSuiteData}
    Set To Dictionary    ${JSONdic}    SummaryTestSuite    ${summary_testsuite}
    Set To Dictionary    ${JSONdic}    SummaryTestCase    ${summary_testcase}
    Set To Dictionary    ${JSONdic}    SummaryAllPass    ${summary_testcasepass}
    Set To Dictionary    ${JSONdic}    SummaryAllFail    ${summary_testcasefail}
    Set To Dictionary    ${JSONdic}    Testsuite    ${listTestsuite}
    Set To Dictionary    ${JSONdic}    TestcaseData    ${TestSuiteData}
    ${json_string}=    Evaluate    json.dumps(${JSONdic})    json
    OperatingSystem.Create File    ${CURDIR}${/}SendEmailReport${/}EmailData.json    ${json_string}
    #Waitng Create File JSON
    OperatingSystem.Wait Until Created    ${CURDIR}${/}SendEmailReport${/}EmailData.json
    sleep    2
    # Replace \ to / support keyword run process
    ${newCurrendDIR}    String.Replace String    ${CURDIR}    ${/}    /
    ${result}    Process.Run Process    ${newCurrendDIR}/SendEmailReport/BatchSendEmailByOutlook.exe    cwd=${newCurrendDIR}/SendEmailReport

*** Keywords ***
Get Testcase Name
    [Arguments]    ${XML}    ${testsuite_name}
    ${testcase_detail}    BuiltIn.Create List
    @{list_testcase_name}    Get Elements    ${XML}    suite/test
    @{list_testcase_status}    Get Elements    ${XML}    suite/test/status
    ${count}    BuiltIn.Get Length    ${list_testcase_name}
    : FOR    ${index}    IN RANGE    1    ${count}+1
    \    ${list_index_xml}    Evaluate    ${index}-1
    \    ${index}    Create Dictionary
    \    ${testcase_name}    Get From Dictionary    ${list_testcase_name[${list_index_xml}].attrib}    name
    \    ${testcase_status}    Get From Dictionary    ${list_testcase_status[${list_index_xml}].attrib}    status
    \    ${testcase_starttime}    Get From Dictionary    ${list_testcase_status[${list_index_xml}].attrib}    starttime
    \    ${testcase_endtime}    Get From Dictionary    ${list_testcase_status[${list_index_xml}].attrib}    endtime
    \    ${duration}    DateTime.Subtract Date From Date    ${testcase_endtime}    ${testcase_starttime}    compact
    \    Set To Dictionary    ${index}    TestSuiteName    ${testsuite_name}
    \    Set To Dictionary    ${index}    Name    ${testcase_name}
    \    Set To Dictionary    ${index}    Status    ${testcase_status}
    \    Set To Dictionary    ${index}    Duration    ${duration}
    \    Append To List    ${testcase_detail}    ${index}
    [Return]    @{testcase_detail}

Get Testsuite Status
    [Arguments]    ${XML}
    ${testsuite_stat}    XML.Get Element    ${XML}    statistics/suite/stat
    ${testsuite_generated}    XML.Get Element    ${XML}
    XML.Log Element    ${testsuite_stat}
    ${testsuite_name}    Collections.Get From Dictionary    ${testsuite_stat.attrib}    name
    ${testsuite_pass}    Collections.Get From Dictionary    ${testsuite_stat.attrib}    pass
    ${testsuite_fail}    Collections.Get From Dictionary    ${testsuite_stat.attrib}    fail
    ${testsuite_total}    BuiltIn.Evaluate    ${testsuite_pass}+${testsuite_fail}
    ${testsuite_total}    BuiltIn.Convert To String    ${testsuite_total}
    ${testsuite_executedate}    Collections.Get From Dictionary    ${testsuite_generated.attrib}    generated
    [Return]    ${testsuite_name}    ${testsuite_pass}    ${testsuite_fail}    ${testsuite_total}    ${testsuite_executedate}

Get Path RobotFramework Log
    ${list_pathlog_folder}    BuiltIn.Create List
    ${pipeline_rootpath_folder}    BuiltIn.Create List
    ${list_pathpipeline_folder}    BuiltIn.Create List
    ${list_suite_folder}    BuiltIn.Create List
    @{jenkins_pipeline}    String.Split String    ${pipeline}    ,
    Log List    ${jenkins_pipeline}
    : FOR    ${pipeline_name}    IN    @{jenkins_pipeline}
    \    log    ${CURDIR}${/}..${/}..${/}..${/}${pipeline_name}${/}result${/}
    \    ${NormalizePath}    OperatingSystem.Normalize Path    ${CURDIR}${/}..${/}..${/}..${/}${pipeline_name}${/}result
    \    ${result} =    Run Process    dir "${NormalizePath}\\*output.xml" /s /b /o:gn    shell=yes    stderr=STDOUT
    \    log many    ${result.stdout}
    \    @{allXmlLog}    Split To Lines    ${result.stdout}
    \    #check continute for loop if folder not exist
    \    log list    ${allXmlLog}
    \    ${list_suite_folder}    Combine Lists    ${list_suite_folder}    ${allXmlLog}
    [Return]    ${list_suite_folder}

Get All Log Path

Get All Log Robotframework Path
    [Arguments]    ${robotframework_xml_path}
    ${list_pathsuite}    BuiltIn.Create List
    #listlog First root
    ${robotframework_suite}    List Directories In Directory    ${robotframework_xml_path}
    log list    ${robotframework_suite}
    : FOR    ${suite_folder}    IN    @{robotframework_suite}
    \    ${suite_folder}    Set Variable    ${robotframework_xml_path}${/}${suite_folder}${/}output.xml
    \    Append To List    ${list_pathsuite}    ${suite_folder}
    \    log list    ${list_pathsuite}
    [Return]    ${list_pathsuite}

Get Summary Testsuite
    [Arguments]    @{list_summary_testsuite}
    ${summary_testsuite}    Get Length    ${list_summary_testsuite}
    ${summary_testcase}    BuiltIn.Set Variable    0
    ${summary_testcasepass}    BuiltIn.Set Variable    0
    ${summary_testcasefail}    BuiltIn.Set Variable    0
    : FOR    ${testsuite_data}    IN    @{list_summary_testsuite}
    \    ${TotalTestcase}    Collections.Get From Dictionary    ${testsuite_data}    TotalTestcase
    \    ${summary_testcase}    BuiltIn.Evaluate    ${summary_testcase}+${TotalTestcase}
    \    ${AllPass}    Collections.Get From Dictionary    ${testsuite_data}    AllPass
    \    ${summary_testcasepass}    BuiltIn.Evaluate    ${summary_testcasepass}+${AllPass}
    \    ${AllFail}    Collections.Get From Dictionary    ${testsuite_data}    AllFail
    \    ${summary_testcasefail}    BuiltIn.Evaluate    ${summary_testcasefail}+${AllFail}
    [Return]    ${summary_testsuite}    ${summary_testcase}    ${summary_testcasepass}    ${summary_testcasefail}
