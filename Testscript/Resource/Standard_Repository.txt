*** Variables ***
@{StanDardRepository}    Full Name    Repo. Name    textbox    txtbox    button    btn    link
...               link    list    list    radio button    radio    tooltip    tooltip
...               checkbox    chkbox    table    table    image    img    label
...               lbl    error message    errmsg    warning message    wrnmsg    title    title
...               drop down list    ddl
@{StanDardTag}    Tag Name    Description    Format    ตรวจสอบรูปแบบอักขระของแต่ละ field    Length    ตรวจสอบความยาวของแต่ละ field    Required
...               ตรวจสอบบังคับกรอกแต่ละ field    DB_XXX    ตรวจสอบ database    DB_Duplicate    ตรวจสอบ database ข้อมูลซ้ำในระบบ    DB_Exist    ตรวจสอบ database ข้อมูลมีอยู่ในระบบ
...               Visible    ตรวจสอบการแสดงของ element บนหน้าจอ    Disable    ตรวจสอบการแสดงของ element บนหน้าจอและไม่สามารถแก้ไขได้    Enable    ตรวจสอบการแสดงของ element บนหน้าจอและสามารถแก้ไขได้    Tooltip
...               ตรวจสอบการแสดงของข้อความ Tooltip    DropdownList    ตรวจสอบการแสดงของข้อมูลใน DropdownList    Default    ตรวจสอบการแสดงของข้อมูล Default กรณีต่างๆ    Calendar    ตรวจสอบการแสดงของข้อมูลปฏิทิน
...               Duplicated    ตรวจสอบการซ้ำกันของข้อมูล    General_XXX    ตรวจสอบรูปแบบการทำงานของหน้าเวปทั่วๆไป
@{StanDardArgument}    Variable    อักษรตัวแรกของคำจะขึ้นต้นด้วยตัวใหญ่เสมอ เช่น ${MobileNo}    Argument    อักษรตัวแรกของคำจะขึ้นต้นด้วยตัวใหญ่เสมอ เช่น ${MobileNo}    Keyword    ขึ้นต้นด้วยตัวใหญ่เสมอไม่แยกคำด้วยเว้นวรรค และห้ามย่อคำเด็ดขาด    Repository
...               ขึ้นต้นเป็นตัวเล็ก (object type ) และขึ้นต้นคำถัดไปเป็นตัวใหญ่เสมอ เช่น btn_ScreenName_ButtonName
