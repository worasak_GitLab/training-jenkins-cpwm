*** Variables ***
${Label_OldPassword}          รหัสผ่านเดิม* :
${Label_NewPassword}          รหัสผ่านใหม่* :
${Label_ConfirmNewPassword}          ยืนยันรหัสผ่านใหม่* :
#Password Format
${Label_PasswordFormat}          รูปแบบของการตั้งรหัสผ่านที่ถูกต้อง
${Label_PasswordCannotBeReuse}          ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิมที่เคยใช้มาแล้ว
${Label_PasswordMustNotBeEasy}          ห้ามตั้งรหัสผ่านที่สามารถคาดเดาได้ง่าย เช่น 1111aaaa หรือเหมือนกับชื่อผู้ใช้งานที่ท่านใช้
${Label_PasswordIsAtLeast8}          รหัสผ่านต้องยาวอย่างน้อย 8 ตัวอักษร
${Label_PasswordMustContain}          รหัสผ่านต้องประกอบด้วยกฏการตั้งรหัสผ่าน 4 ข้อ ตามรายละเอียดดังนี้
${Label_Upper(A-Z)}          มีภาษาอังกฤษตัวพิมพ์ใหญ่ (A-Z)
${Label_Lower(a-z)}          มีภาษาอังกฤษตัวพิมพ์เล็ก (a-z)
${Label_Digits(0-9)}          มีตัวเลข อารบิค (0-9)
${Label_Alphanumeric}          มีอักขระพิเศษ (สำหรับระบบ Plugin ให้ใช้ @ หรือ # เท่านั้น)
