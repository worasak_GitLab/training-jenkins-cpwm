*** Settings ***
Resource          Common_Keywords.txt
Resource          ../PageRepositories/ResetPasscode_PageRepositories.txt
Resource          ../PageVariables/ResetPasscode_PageVariables.txt
Resource          ../Localized/${ar_Env}/Label_ResetPasscode_Localized.txt
Resource          ../Localized/${ar_Env}/Error_ResetPasscode_Localized.txt
Resource          ../Localized/${ar_Env}/Remark_ResetPasscode_Localized.txt
Resource          Register_PageKeywords.txt

*** Keywords ***
wApplicationResetPasscode

wBackOfficeResetPasscode

wCheckStatusResetPasscode
    [Arguments]    ${Username}    ${Data_Register_User}
    [Documentation]    This keyword for Check Status ResetPasscode\ and Input New Register Data if Process ResetPasscode Fail
    ...
    ...    *Format Keyword*
    ...
    ...    wCheckStatusResetPasscode | ${Username} | ${Data_Register_User}
    log    ${Data_Register_User}
    log    ${Username}
    ## Work around Register Fail
    ${Data}    wQueryDataBaseCPWM    SELECT * FROM cpwm."UserRegister" \ WHERE "UserID" = (SELECT "UserID" FROM cpwm."User" WHERE "Username" = '${Username}');
    ${Status}    BuiltIn.Run Keyword And Return Status    BuiltIn.Should Be Empty    ${Data}
    Log    ${Status}
    BuiltIn.Run Keyword If    '${Status}' == 'True'    wRegisterProcessComplete    ${Data_Register_User}

wEmployeeInputDataResetPasscode
    [Arguments]    ${Data_User_ResetPasscode}
    [Documentation]    This keyword for input Reset Passcode and return input value data
    ...
    ...
    ...    *Format Keyword*
    ...
    ...    ${Dict_ResetPasscode} | wEmployeeInputDataResetPasscode | ${Data_User_ResetPasscode}
    ${Data}    Get Dictionary Items    ${Data_User_ResetPasscode}
    log    ${Data}
    ${Dict_ResetPasscode}    BuiltIn.Create Dictionary
    : FOR    ${Key}    ${Value}    IN    @{Data}
    \    ${Key}    Convert To Lowercase    ${Key}
    \    ${Key}    BuiltIn.Run Keyword If    '${Key}' == 'pinempid'    BuiltIn.Set Variable    pin
    \    ...    ELSE IF    '${Key}' == 'confirmnewpasscode'    BuiltIn.Set Variable    confirmpasscode
    \    ...    ELSE    BuiltIn.Set Variable    ${Key}
    \    ${StatusRef.Code}    BuiltIn.Run Keyword And Return Status    Page Should Contain Element    ${txt_ResetPasscode_Refcode}
    \    ${RefNo.}    BuiltIn.Run Keyword If    '${StatusRef.Code}' == 'True'    Get Web Text    ${txt_ResetPasscode_Refcode}
    \    ${OTPRefNo.}    BuiltIn.Run Keyword If    '${StatusRef.Code}' == 'True' and '${RefNo.}' != ''    Fetch From Left    ${RefNo.}    ${SPACE}
    \    BuiltIn.Run Keyword If    '${StatusRef.Code}' == 'True' and '${OTPRefNo.}' != ''    Set To Dictionary    ${Dict_ResetPasscode}    REFERENCECODE=${OTPRefNo.}
    \    ## Verify Ref Code Reset Passcode
    \    ${Data}    BuiltIn.Run Keyword If    '${StatusRef.Code}' == 'True' and '${RefNo.}' != ''    wQueryDataBaseCPWM    SELECT * FROM cpwm."User" Where "Username" = '${Data_User_ResetPasscode['Username']}';
    \    ${UserID}    BuiltIn.Run Keyword If    '${StatusRef.Code}' == 'True' and '${RefNo.}' != ''    BuiltIn.Set Variable    ${Data[0]['UserID']}
    \    ${Data_ResetPasscode}    BuiltIn.Run Keyword If    '${StatusRef.Code}' == 'True' and '${RefNo.}' != ''    wQueryDataBaseCPWM    SELECT * FROM cpwm."SMS" WHERE "UserID"='${UserID}' AND "SMSType"='OTP';
    \    BuiltIn.Run Keyword If    '${StatusRef.Code}' == 'True' and '${RefNo.}' != ''    BuiltIn.Should Be Equal    ${Data_ResetPasscode[0]['ReferenceCode']}    ${OTPRefNo.}
    \    ## Verify End
    \    ${StatusXpath}    BuiltIn.Run Keyword And Return Status    Page Should Contain Element    xpath=//input[@id="${Key}"]
    \    ${StatusValue}    BuiltIn.Run Keyword And Return Status    Page Should Contain Element    xpath=//input[@value="${Value}"]
    \    BuiltIn.Run Keyword If    '${StatusXpath}' == 'True' and '${StatusValue}' == 'False'    BuiltIn.Run Keywords    Wait Until Element Is Visible    xpath=//input[@id="${Key}"]    ${General Timeout}
    \    ...    AND    Input Text    xpath=//input[@id="${Key}"]    ${Value}
    \    ...    AND    sleep    2s
    \    ...    ELSE    Log    No Field/Disable.
    \    ${Key}    Convert To Uppercase    ${Key}
    \    BuiltIn.Run Keyword If    '${Key}' == 'CONFIRMPASSCODE'    Set To Dictionary    ${Dict_ResetPasscode}    PASSCODE=${Value}
    \    BuiltIn.Run Keyword If    '${Key}' == 'NEWPASSCODE'    Set To Dictionary    ${Dict_ResetPasscode}    SALT=${Value}
    \    Set To Dictionary    ${Dict_ResetPasscode}    ${Key}=${Value}
    Capture Page Screenshot
    log    ${Dict_ResetPasscode}
    [Return]    ${Dict_ResetPasscode}

wEmployeeResetPasscode

wRecheckOTPEmptyAfterResetPasscode
    [Arguments]    ${Username}
    [Documentation]    This keyword for Recheck OTP empty after reseted passcode.
    ...
    ...
    ...    *Format Keyword*
    ...
    ...    wRecheckOTPEmptyAfterResetPasscode | ${Username}
    ${Username}    Convert To Lowercase    ${Username}
    ${QueryOTP_Recheck}    wQueryDataBaseCPWM    SELECT * FROM cpwm."SMS" WHERE "UserID" = (SELECT "UserID" FROM cpwm."User" WHERE "Username" = '${Username}');
    log    ${QueryOTP_Recheck}
    BuiltIn.Should Be Empty    ${QueryOTP_Recheck}
    log    ======== OTP is empty on database. ========

wRoboticResetPasscode
